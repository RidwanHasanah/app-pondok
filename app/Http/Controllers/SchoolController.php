<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\School;

class SchoolController extends Controller
{
    public function create()
    {
        $schools = School::all();

        return view('setting.userSetting', compact('schools'));
    }

    public function store()
    {
        /* Validasi Start*/
        //   $this->validate(request(), [
        //   'name'    => 'required|min:5',
        //   'address' => 'required|min:50'
        // ]);
        /* Validasi End*/

        School::create([
          'name'       => request('name'),
          'address'    => request('address')
        ]);

        return redirect()->route('setting.create');//->withInfo('School Aded');//->with('success','Post aded');
        //->with('success','Post aded');  untuk membuat alert jika berhasil membuat post
    }
}
