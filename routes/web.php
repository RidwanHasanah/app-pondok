<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::get('login', function () {
    return view('login');
})->name('login');

Route::get('list', function () {
    return view('list');
})->name('list');

Route::get('user', function () {
    return view('user');
})->name('user');

Route::prefix('setting')->group(function () {
    Route::get('/', function () {
        return view('setting.index');
    })->name('setting');

    Route::get('user', 'SchoolController@create')->name('setting.create');
    Route::post('user', 'SchoolController@store')->name('setting.store');
    /*Route::get('user', function () {
        return view('setting.userSetting');
    })->name('setting.user');*/
});

Route::prefix('admin-panel')->group(function () {
    Route::get('/', function () {
        return view('admin.index');
    })->name('admin');

    Route::get('manage-santri', function () {
        return view('admin.manageSantri');
    })->name('admin.santri');

    Route::get('manage-faculty', function () {
        return view('admin.manageFaculty');
    })->name('admin.faculty');

    Route::get('manage-ineterest', function () {
        return view('admin.manageInterest');
    })->name('admin.interest');
});
