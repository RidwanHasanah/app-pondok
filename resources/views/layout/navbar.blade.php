<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar bar1"></span>
                <span class="icon-bar bar2"></span>
                <span class="icon-bar bar3"></span>
            </button>
            <a class="navbar-brand">
                {{-- NAVBAR TITLE --}}
                @yield('nav-title')
            </a>
            <ul class="nav navbar-nav">
                @yield('navbar-item')
            </ul>
        </div>

        {{-- COLLAPSE MOBILE VIEW --}}
        <div class="collapse navbar-collapse">
        </div>
    </div>
</nav>
