<!--
    Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
    Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
-->
<div class="sidebar" data-background-color="inverse" data-active-color="primary">
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="" class="simple-text">
                <img style="width: 65%;" src="/assets/img/pondok/logo.png" alt="Pondok IT">
            </a>
        </div>

        <ul class="nav">
            <li>
                <a href="{{ route('user') }}">
                    <i class="ti-user"></i>
                    <p>User</p>
                </a>
            </li>
            <li>
                <a href="{{ route('list') }}">
                    <i class="ti-view-list-alt"></i>
                    <p>Santri Lists</p>
                </a>
            </li>
            <li>
                <a href="{{ route('setting') }}">
                    <i class="ti-settings"></i>
                    <p>Setting</p>
                </a>
            </li>

            {{-- CHECK IF ADMIN --}}
            {{-- @if (Auth::check()) --}}
                <li>
                    <a href="{{ route('admin') }}">
                        <i class="ti-settings"></i>
                        <p>Admin Panel</p>
                    </a>
                </li>
            {{-- @endif --}}

            <li>
                <a href="">
                    <i class="fa fa-sign-out"></i>
                    <p>Logout</p>
                </a>
            </li>
        </ul>
    </div>
</div>
