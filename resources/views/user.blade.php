@extends('layout.master')

@section('nav-title')
    My Profile
@endsection

@section('content')

<div class="container-fluid">
    <div class="row">
        <div style="width: 92%; margin: 0 auto">
            <div class="card card-user">
                <div class="image">
                    <img src="/assets/img/background.jpg" alt="..."/>
                </div>
                <div class="content">
                    <div class="author">
                      <img class="avatar border-white" src="/assets/img/faces/face-1.jpg" alt="..."/>

                    {{-- NAMA SANTRI --}}
                      <h4 class="title"> FULL NAME <br />
                         <a href="#"><small>@nickname</small></a>
                      </h4>

                    </div>
                    <p class="description text-center">
                        "I like the way you work it"
                    </p>
                    <h4 class="faculty">Faculty:</h4>
                    <div class="img-faculty">
                        <img class="pondok" src="/assets/img/pondok/pondokprogrammer.png" alt="Pondok IT">
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </div>
</div>

@endsection
