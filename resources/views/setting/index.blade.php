@extends('layout.master')

@section('nav-title')
    Setting
@endsection

@include('setting.layout.navbar-item')

@section('content')

    <div style="text-align: center; padding-top: 7vh; color: #D6D6D6">
        <h1>Welcome to setting</h1>
        <i style="font-size: 50px" class="ti-settings"></i>
    </div>

@endsection
