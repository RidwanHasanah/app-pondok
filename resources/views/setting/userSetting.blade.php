@extends('layout.master')

@section('nav-title')
    Setting
@endsection

@include('setting.layout.navbar-item')

@section('content')

    <div class="card" style="width: 85%; margin: 0 auto">
        <div class="header" style="text-align: center;">
            <h3><i class="ti-user"></i> My Profile</h3>
        </div>
        <div class="content">

            <form>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Full Name</label>
                            <input type="text" class="form-control border-input" placeholder="Full Name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nick Name</label>
                            <input type="text" class="form-control border-input" placeholder="Nick Name">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Faculty</label>
                            <input type="text" class="form-control border-input" placeholder="Faculty">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>School</label>
                            <input readonly type="text" class="form-control border-input" value="">
                        </div>
                    </div>
                </div>

                <div class="text-center">
                    <button type="submit" class="btn btn-info btn-fill btn-wd">Update Profile</button>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>

<div style="margin-top: 5vh">
    <div class="card" style="width: 85%; margin: 0 auto">
        <div class="header" style="text-align: center;">
            <h3><i class="ti-home"></i> My School Profile</h3>
        </div>
        <div class="content">

            <form action="{{ route('setting.store') }}" method="post">
              {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>School Name</label>
                            <input type="text" name="name" value="{{ old('name') }}" class="form-control border-input" placeholder="School Name">
                            @if ($errors->has('name'))
                              <span class="help-block">
                                <p>{{ $errors ->first('name') }}</p>
                              </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Adress</label>
                            <input value="{{ old('address') }}" type="text" name="address" class="form-control border-input" placeholder="School Adress">
                            @if ($errors->has('address'))
                              <span class="help-block">
                                <p>{{ $errors ->first('address') }}</p>
                              </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="text-center">
                    <button type="submit" class="btn btn-info btn-fill btn-wd">Save</button>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>
</div>


@endsection
