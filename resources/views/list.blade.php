@extends('layout.master')

@section('nav-title')
    Santri Lists
@endsection

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h4 class="title" style="text-align: center;">Daftar Santri di Pondok IT</h4>
                </div>
                <div class="content table-responsive table-full-width">
                    <table class="table table-striped">
                        <thead>
                            <th>No</th>
                            <th>Name</th>
                            <th>Faculty</th>
                            <th>School</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Dakota Rice</td>
                                <td>Pondok Programmer</td>
                                <td>SMK N 1 Indonesia</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Dakota Rice</td>
                                <td>Pondok Programmer</td>
                                <td>SMK N 1 Indonesia</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Dakota Rice</td>
                                <td>Pondok Programmer</td>
                                <td>SMK N 1 Indonesia</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Dakota Rice</td>
                                <td>Pondok Programmer</td>
                                <td>SMK N 1 Indonesia</td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>
</div>

@endsection
