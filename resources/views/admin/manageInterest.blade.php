@extends('layout.master')

@section('nav-title')
    Admin Panel
@endsection

@include('admin.layout.navbar-item')

@section('content')


<div class="container-fluid">
    <div class="row">
        <div class="col-lg-8 col-md-7">
            <div class="content">
                <div class=" table-responsive table-full-width">
                <table class="table table-striped">
                    <thead>
                        <th style="text-align: center;">ID</th>
                        <th style="text-align: center;">Interest</th>
                        <th style="text-align: center;" colspan="2">Options</th>
                    </thead>
                    <tbody>
                        <tr style="text-align: center;">
                            <td>1</td>
                            <td>PHP</td>
                            <td><a href="">Edit</a></td>
                            <td><a href="">Delete</a></td>
                        </tr>
                    </tbody>
                </table>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-5">
            <div class="card">
                <div class="content">
                    <form>
                        <div class="form-group">
                            <input type="text" class="form-control border-input" placeholder="Interest">
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-info btn-fill btn-wd">Add New Interest</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
