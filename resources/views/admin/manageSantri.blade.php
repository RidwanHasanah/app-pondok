@extends('layout.master')

@section('nav-title')
    Admin Panel
@endsection

@include('admin.layout.navbar-item')

@section('content')


<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h4 class="title" style="text-align: center;">Tambah Santri Pondok IT</h4>
                </div>
                <div class="content">
                    <form>
                        <form>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" class="form-control border-input" placeholder="Full Name">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Faculty</label>
                                        <input type="text" class="form-control border-input" placeholder="Faculty">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Interest</label>
                                        <input type="text" class="form-control border-input" placeholder="Interest">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>School</label>
                                        <input type="text" class="form-control border-input" placeholder="School">
                                    </div>
                                </div>
                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-info btn-fill btn-wd">Add Santri</button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h4 class="title" style="text-align: center;">Daftar Santri Pondok IT</h4>
                </div>
                <div class="content table-responsive table-full-width">
                    <table class="table table-striped">
                        <thead>
                            <th>No</th>
                            <th>Name</th>
                            <th>Faculty</th>
                            <th>School</th>
                            <th style="text-align: center;" colspan="2">Options</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Dakota Rice</td>
                                <td>Pondok Programmer</td>
                                <td>SMK N 1 Indonesia</td>
                                <td style="text-align: center;"><a href="">Edit</a></td>
                                <td style="text-align: center;"><a href="">Delete</a></td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>
</div>

@endsection
