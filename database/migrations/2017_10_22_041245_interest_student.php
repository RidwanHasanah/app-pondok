<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InterestStudent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interest_student', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('student_id');
            $table->integer('interest_id')->unsigned();

            $table->timestamps();

            $table->foreign('interest_id')->references('id')->on('interests');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
