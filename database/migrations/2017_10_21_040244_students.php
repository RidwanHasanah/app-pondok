<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Students extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('fullname');
            $table->string('nickname');
            $table->date('entry_date');
            $table->integer('school_id')->unsigned();
            $table->integer('facultas_id')->unsigned();
            $table->timestamps();


            $table->foreign('school_id')->references('id')->on('schools');
            $table->foreign('facultas_id')->references('id')->on('facultas');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
